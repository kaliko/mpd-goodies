#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import glob

from setuptools import setup

# cf. setup.cfg
setup(
    scripts=glob.glob('bin/*'),
)

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab
